import React, { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import Cartbtn from "./Cartbtn";

function DynanProd() {
  const [prodDetails, setprodDetails] = useState([]);
  const { isLoggedInUser } = useSelector((state) => state.auth);
  useEffect(() => {
    axios
      .get("http://localhost:4000/webapiuser/fetchProdDetails")
      .then((res) => {
        console.log(res);
        setprodDetails(res.data);
      });
  }, [setprodDetails]);

  return (
    <div>
      <div class="ourproduct">
        <div class="container">
          <div class="row product_style_3">
            {prodDetails.map((s) => (
              <div class=" col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="full product">
                  <div class="product_img">
                    <div class="center">
                      <img src={`/prodimages/${s.prodimg}`} alt="#" />
                      <div class="overlay_hover">
                        {isLoggedInUser && (
                          <Cartbtn
                            pname={s.pname}
                            prodimg={s.prodimg}
                            price={s.price}
                          />
                        )}
                        {isLoggedInUser && (
                          <buy
                            pname={s.pname}
                            prodimg={s.prodimg}
                            price={s.price}
                          />
                        )}
                      </div>
                    </div>
                  </div>
                  <div class="product_detail text_align_center">
                    <p class="product_price">
                      ₹{s.price} <span class="old_price">₹{s.price + 100}</span>
                    </p>
                    <p class="product_descr">{s.pname}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default DynanProd;
