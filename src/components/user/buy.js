/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import Product from "../products/product";
import { Link } from "react-router-dom";

function Buy() {
  const { isLoggedInUser } = useSelector((state) => state.auth);
  const [plist, setPlist] = useState([]);
  const history = useHistory();
  const pno = plist.length;
  console.log(pno);
  const confirm = () => {
    alert("Your Order Is Confirmed. Thank You ;)");
    localStorage.removeItem("cart");
    history.push("/product");
  };

  useEffect(() => {
    var data = JSON.parse(localStorage.getItem("cart"));
    setPlist(data);
  }, [setPlist]);

  if (isLoggedInUser) {
    return (
      <>
        <div className="contactus">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 offset-md-2">
                <div className="title">
                  <h2>Billing And Payment</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <table className="table table-bordered">
            <tr>
              <th>Product Title</th>
              <th>Product Image</th>
              <th>Product Price</th>
            </tr>

            {plist.map((p, index) => (
              <tr key={index}>
                <td>{p.ProductTitle}</td>
                <td>
                  <img
                    src={`/product/${p.ProductImage}`}
                    width="50%"
                    heigth="50%"
                  ></img>
                </td>
                <td>Rs.{p.ProductPrice}</td>
              </tr>
            ))}
          </table>
          <p>
            <h4>Billing Details</h4>
          </p>
          <table className="table table-bordered">
            <tr>
              <th>EMAIL</th>
              <td>{localStorage.getItem("email")}</td>
            </tr>
            <tr>
              <th>NAME</th>
              <td>{localStorage.getItem("name")}</td>
            </tr>
            <tr>
              <th>CONTACT NO</th>
              <td>{localStorage.getItem("phone")}</td>
            </tr>
            <tr>
              <th>ADDRESS</th>
              <td>{localStorage.getItem("address")}</td>
            </tr>
            <tr>
              <th>ORDER</th>
              <td>
                {plist.map((p, index) => (
                  <tr key={index}>
                    <td>{p.ProductTitle}</td>
                    <td>Rs.{p.ProductPrice}</td>
                  </tr>
                ))}
              </td>
            </tr>

            <th>TOTAL</th>

            <td style={{ color: "green" }}>
              Rs.
              {plist.reduce(
                (total, index) => (total = total + index.ProductPrice),
                0
              )}
            </td>
            <tr>
              <th>SHIPPING</th>
              <td>3 to 4 business days</td>
            </tr>
            <tr>
              <th>PAYMENT METHOD</th>
              <td>Cash On Delivery</td>
            </tr>
          </table>
          <button
            type="submit"
            className="btn btn-default"
            className="block-example border border-dark"
            style={{ color: "green" }}
            onClick={confirm}
          >
            Confirm Order
          </button>
          <br />
          <br />
          <br />
          <h4>Do You Like to Continue Shopping</h4>
          <button className="btn btn-default">
            <Link to="product">Continue Shopping</Link>
          </button>
        </div>
      </>
    );
  }
}

export default Buy;
